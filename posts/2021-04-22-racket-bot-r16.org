#+setupfile: ../org-template/style.org
#+title: NOTE: Deploy Racket Bot r16 on Heroku
#+date: <2021-04-22>

How to deploy r16 bot on Heroku.

#+begin_src shell
git clone git@github.com:williewillus/r16.git
cd r16/
echo 'web: racket -l r16 ./' > Procfile
heroku buildpacks:set https://github.com/lexi-lambda/heroku-buildpack-racket
heroku config:set RACKET_VERSION=8.0
heroku config:set BOT_TOKEN=<your bot token>
git push heroku master
#+end_src
