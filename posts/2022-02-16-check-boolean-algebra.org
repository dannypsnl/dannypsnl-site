#+setupfile: ../org-template/style.org
#+title: indexing boolean algebra
#+date: <2022-02-16>

Boolean algebra can be encoded by indexing, for example, a venn diagram
for two sets: \(A\) and \(B\) divide diagram to four parts:
\(1, 2, 3, 4\). Thus

1. \(A = \{1, 2\}\)
2. \(B = \{1, 3\}\)
3. \(A \cup B = \{1, 2, 3\}\)
4. \(A \cap B = \{1\}\)
5. \((A \cup B)' = \{4\}\)

The benefit of the encoding is the encoding let any sets can be treated
as finite sets operation. So for any set check a boolean algebra is
valid, indexing helps you check them by computer.

*** Encoding to program
:PROPERTIES:
:CUSTOM_ID: encoding-to-program
:END:
#+begin_src racket
(define I (set 1 2 3 4))
(define A (set 1 2))
(define B (set 1 3))
(define ∅ (set))
(define ∩ set-intersect)
(define ∪ set-union)
(define (not S)
  (set-subtract I S))
(define (→ A B)
  (∪ (not A) B))
(define (- A B)
  (∩ A (not B)))
(define (≡ A B)
  (∪ (∩ A B) (∩ (not A) (not B))))
#+end_src

*** Checking based on encoding
:PROPERTIES:
:CUSTOM_ID: checking-based-on-encoding
:END:
Now, you can check \(A \to B = (A - B)'\) by the following program.

#+begin_src racket
(equal? (→ A B) (not (- A B)))
#+end_src

And you will know \((A \cap (A - B)') = \emptyset\) is invalid since the
following program returns false.

#+begin_example
(equal? (∩ A (not (- A B))) ∅)
#+end_example

*** Futher
:PROPERTIES:
:CUSTOM_ID: futher
:END:
You can even extend this method for \(A, B, C\), for \(A, B, C, D\) and
so on, but then that's too far for this article.
