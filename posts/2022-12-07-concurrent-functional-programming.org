#+setupfile: ../org-template/style.org
#+title: NOTE: functional programming 對執行區塊上鎖
#+date: <2022-12-07>

functional programming
中讓一個函數執行時與特定鎖結合可以用高階函式確保這個模式不被意外打破

#+begin_src ocaml
let withLock : mutex -> (a -> b) -> a -> b =
  fun mu f x -> acquire mu; let r = f x in release mu; r
#+end_src

這件事好玩之處在很容易觀察到下面的用法

#+begin_src ocaml
withLock mu
  (fun _ -> ...)
  ()
#+end_src

進而衍伸出讓區塊轉化成匿名函數的語法糖

#+begin_src ocaml
withLock mu { ... }
#+end_src

像是把上面的 ={ ... }= 轉化成 =fun _ -> ...=。實務上 Kotlin
就有類似的轉換。
