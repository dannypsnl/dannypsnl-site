#+setupfile: ../org-template/style.org
#+title: NOTE: bytes encoding conversion in Racket
#+date: <2022-07-14>

The following code records how to convert bytes's encoding in Racket,
from MS950(CP950) to UTF-8

#+begin_src racket
(define-values (result success-length status)
  (bytes-convert (bytes-open-converter "CP950" "UTF-8")
                 #"A888º£BC"))

result
success-length
status
#+end_src
