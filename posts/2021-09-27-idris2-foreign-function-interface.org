#+setupfile: ../org-template/style.org
#+title: idris2 FFI
#+date: <2021-09-27>

Idris has a simple FFI, the following code is an example.

#+begin_src idris
%foreign "C:puts,libc"
prim__puts : String -> PrimIO Int
#+end_src

To avoid duplicate typing, we usually would write

#+begin_src idris
libc : String -> String
libc fname = "C:" ++ fname ++ ",libc"

%foreign libc "puts"
prim__puts : String -> PrimIO Int
#+end_src

Then we would like to wrap =puts= as more general type. Thus, we get

#+begin_src idris
puts : HasIO io => String -> io Int
puts s = primIO $ prim__puts s
#+end_src
