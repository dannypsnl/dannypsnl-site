#+setupfile: ../org-template/style.org
#+title: setup libraries for chez scheme
#+date: <2022-01-14>

Since I finally figure out how to set up these, I record them and
provide some convenience common setup.

It's all about environment variable: =CHEZSCHEMELIBDIRS=, it's a common
shell path list in =<path>:<path>:<path>:...= format.

First, create a directory under homepath named =chezscheme-lib=.

*** Nanopass
:PROPERTIES:
:CUSTOM_ID: nanopass
:END:
Now, you have a directory named =chezscheme-lib= under homepath. You can
get nanopass framework from github.

#+begin_src shell
git clone git@github.com:nanopass/nanopass-framework-scheme.git
#+end_src

Then run

#+begin_src shell
export CHEZSCHEMELIBDIRS="$HOME/chezscheme-lib/nanopass-framework-scheme/:"
#+end_src

Now =(import (nanopass))= will work.

*** srfi
:PROPERTIES:
:CUSTOM_ID: srfi
:END:
I found a srfi for chez on github, as you just do, first clone it.

#+begin_src shell
git clone git@github.com:arcfide/chez-srfi.git
#+end_src

One more step required

#+begin_src shell
./chez-srfi/install.chezscheme.sps $HOME/chezscheme-lib
#+end_src

Finally, we export it

#+begin_src shell
export CHEZSCHEMELIBDIRS="$HOME/chezscheme-lib/:$CHEZSCHEMELIBDIRS"
#+end_src
