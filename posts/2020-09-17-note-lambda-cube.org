#+setupfile: ../org-template/style.org
#+title: NOTE: Lambda Cube
#+date: <2020-09-17>

First we have UTLC(untyped lambda calculus) to STLC(simply typed lambda
calculus), by adding arrow type(\(\to\)): \(\lambda x:Nat.x\)

*** Lambda cube
:PROPERTIES:
:CUSTOM_ID: lambda-cube
:END:
Lambda cube starts from STLC.

**** STLC -> \(\lambda 2\)
:PROPERTIES:
:CUSTOM_ID: stlc---lambda-2
:END:
Terms depend on Types: \(\lambda (a : *).\lambda (x:a).x\)

**** \(\lambda 2\) -> \(\lambda \omega\)
:PROPERTIES:
:CUSTOM_ID: lambda-2---lambda-omega
:END:
Types depend on Types: \(\lambda (a : *).a \to a\)

**** \(\lambda 2\) -> \(\lambda \Pi\) (\(\Pi\) type)
:PROPERTIES:
:CUSTOM_ID: lambda-2---lambda-pi-pi-type
:END:
Types depend on Terms: \(\Pi (x : a).M\)

**** COC(calculus of construction)
:PROPERTIES:
:CUSTOM_ID: coccalculus-of-construction
:END:
Mix \(\Pi\) and \(\lambda\), type is term.

COC = \(\lambda 2\) + \(\lambda \omega\) + \(\lambda \Pi\)

**** CIC(calculus of inductive construction)
:PROPERTIES:
:CUSTOM_ID: ciccalculus-of-inductive-construction
:END:
Introduce Inductive, e.g. =data Nat = zero | suc Nat=

CIC = COC + Inductive
