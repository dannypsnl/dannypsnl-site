#+setupfile: ../org-template/style.org
#+title: How to get start with Rust
#+date: <2018-05-26 Sat>

Rust is a strange language.

** Moving
:PROPERTIES:
:CUSTOM_ID: moving
:END:
First point is move semantic.

#+begin_src rust
fn main() {
    let s = "hello".to_string();
    let t = s;
    let u = s;
}
#+end_src

What do you expect? =t= & =u= is =s=? No! =rustc= says:

#+begin_example
error[E0382]: use of moved value: `s`
 --> main.rs:4:9
  |
3 |     let t = s;
  |         - value moved here
4 |     let u = s;
  |         ^ value used here after move
  |
  = note: move occurs because `s` has type `std::string::String`, which does not implement the `Copy` trait
#+end_example

This error tell you why you got a fail. In Rust, you should expect
default behavior of assign is moving!

*** Copy
:PROPERTIES:
:CUSTOM_ID: copy
:END:
You can expect if any type implement the =Copy= trait, will be copied.
That's why these code will work:

#+begin_src rust
let s = 1;
let t = s;
let u = s;
#+end_src

Implement the =Copy= trait can use =derive= notation in Rust.

#+begin_src rust
#[derive(Clone, Copy)]
struct Foo {
    number: i32
}
#+end_src

Notice that =Clone= is required. However, a type implement =Copy= can't
have a field do not implement =Copy=!!! So following code will fail.

#+begin_src rust
#[derive(Clone, Copy)]
struct Foo {
    label: String
}
#+end_src

Error message:

#+begin_example
error[E0204]: the trait `Copy` may not be implemented for this type
 --> main.rs:1:17
  |
1 | #[derive(Clone, Copy)]
  |                 ^^^^
2 | struct Foo {
3 |     number: String,
  |     -------------- this field does not implement `Copy`
#+end_example

** Mutable
:PROPERTIES:
:CUSTOM_ID: mutable
:END:
In Rust, mutable and immutable is very different.

#+begin_src rust
let s = "one ".to_string();
s.push_str("two ");
#+end_src

Error report:

#+begin_example
error[E0596]: cannot borrow immutable local variable `s` as mutable
 --> main.rs:3:5
  |
2 |     let s = "one ".to_string();
  |         - consider changing this to `mut s`
3 |     s.push_str("two ");
  |     ^ cannot borrow mutably
#+end_example

Because =String::push_str= borrow =&mut self=, it can't be used by an
immutable =String=.

** Reference
:PROPERTIES:
:CUSTOM_ID: reference
:END:
We have already seen =String=, it's called *owning pointer*, others are
=Vec=, =Box=, etc. However, these pointers will move data. Sometime, we
don't need to moving data, but also don't want to copy it. It's
reference showtime! Rust's reference has some points.

1. explicitly using thing it point to

#+begin_src rust
let a = 1;
let r = &a;
assert!(*r == 1);
#+end_src

2. [@2] mutable reference to mutable ownership

#+begin_src rust
let mut num = 15;
let r = &mut num;
*r += 10;
assert!(*r == 25);
#+end_src

3. [@3] references are never null
4. you can't borrow a value will outlive when you still alive

#+begin_src rust
let r;
{
    let x = 1;
    r = &x;
}
#+end_src

Error message:

#+begin_example
error[E0597]: `x` does not live long enough
 --> main.rs:5:14
  |
5 |         r = &x;
  |              ^ borrowed value does not live long enough
6 |     }
  |     - `x` dropped here while still borrowed
7 | }
  | - borrowed value needs to live until here
#+end_example

The problem is =r= can be access after =x= already be dropped! That mean
a dangling pointer. Rust disallowed it.

** Conclusion
:PROPERTIES:
:CUSTOM_ID: conclusion
:END:
I think these are the hardest part when you are beginner of Rust.
Because Rust chooses a new way to handle its memory, move default,
checking live-time, sharing data by reference. Understanding these
things is most important to get familiar with Rust. Hope you like it &
can get some help from this.

*** References:
:PROPERTIES:
:CUSTOM_ID: references
:END:
**** [[http://shop.oreilly.com/product/0636920040385.do][Programming Rust]]
:PROPERTIES:
:CUSTOM_ID: programming-rust
:END:
- Author: Jim Blandy & Jason Orendorff
- ISBN: 978-1-491-92728-1
