#+setupfile: ../org-template/style.org
#+title: NOTE: resource algebra
#+date: <2022-12-19>

Note about how to write limitation about resource usage to form
different restriction

1. affine =0..1=: at most once
2. linear =1..1=: exactly once
3. relevant =1..∞=: at least once

This annotation also release the power that we can also have =1..3= or
=0..7=, what a wonderful idea.
