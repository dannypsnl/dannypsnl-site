all: build
.PHONY: all

build:
	@echo "Building... with current Emacs configurations."
	emacs --batch --load publish.el --funcall org-publish-all
.PHONY: build

push: build
	(cd dist/; git add .; git commit -m "update"; git push)
.PHONY: push

clean:
	@echo "Cleaning up.."
	@rm -rvf *.elc
	@rm -rvf dist
	@rm -rvf ~/.org-timestamps/*
.PHONY: clean
