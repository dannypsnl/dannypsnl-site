---
iscard: "true"
tags:
  - racket
  - agda
  - arend
title: "plt research"
link: "https://github.com/dannypsnl/plt-research"
---

Collection of Programming Language Theory researches.
