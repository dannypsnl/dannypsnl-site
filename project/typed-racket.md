---
iscard: "true"
tags:
  - typed racket
title: "typed-racket"
link: "https://github.com/racket/typed-racket"
---

- Let several functions of `racket/flonum` type checked to behave same as in `#lang racket`
  - [PR 1294](https://github.com/racket/typed-racket/pull/1294)
- Add missing syntax locations in an error report
  - [PR 999](https://github.com/racket/typed-racket/pull/999)
