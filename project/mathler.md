---
iscard: "true"
tags:
  - racket
  - mathler
  - puzzle
  - game
title: "mathler"
link: "https://github.com/dannypsnl/mathler"
---

terminal version of [mathler game](https://www.mathler.com), also a solver for mathler game.
