---
iscard: "true"
tags:
  - go
  - web
title: "rocket"
link: "https://github.com/dannypsnl/rocket"
---

Light-weight web framework for Go

```go
type User struct {
  Name string `route:"name"`
  Age  uint64 `route:"age"`
}
func hello(u *User) string { return "Hello " + u.Name + ", your age is " + strconv.FormatUint(u.Age, 10) }

rocket.Ignite(8080).
  Mount(
    rocket.Get("/user/:name/:age", hello),
  ).
  Launch()
```
