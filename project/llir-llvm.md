---
iscard: "true"
tags:
  - llvm
  - llir
  - go
title: "llir/llvm"
link: "https://github.com/llir/llvm"
---

Library for interacting with LLVM IR in pure Go.
As a contributor, I majorly contribute to

- 128-bits floating number
- support of llvm 10 to 15
- [document](https://llir.github.io/document/)
- [extend](https://github.com/dannypsnl/extend) project that improving using experience
