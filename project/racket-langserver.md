---
iscard: "true"
tags:
  - working
  - racket
  - language server
title: "racket-langserver"
link: "https://github.com/jeapostrophe/racket-langserver"
---

Racket language server

- [implement cross-file jump to definition](https://github.com/jeapostrophe/racket-langserver/pull/59)
- [formatting: remove trailing whitespace](https://github.com/jeapostrophe/racket-langserver/pull/61)
- [implementing `inlayHint`](https://github.com/jeapostrophe/racket-langserver/pull/84)
- [unused variable: diagnostic & code action](https://github.com/jeapostrophe/racket-langserver/pull/87)
- working on: [implementing `willRenameFiles`](https://github.com/jeapostrophe/racket-langserver/pull/83)
