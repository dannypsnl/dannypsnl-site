---
iscard: "true"
tags:
  - go
  - redux
title: "redux"
link: "https://github.com/dannypsnl/redux"
---

redux in Go

```go
type CountingModel struct {
  rematch.Reducer
  State int
  Increase *rematch.Action `action:"IncreaseImpl"`
}
func (c *CountingModel) IncreaseImpl(s, payload int) int { return s + payload }

c := &CountingModel {
    State: 0,
}
store := store.New(c)
store.Dispatch(c.Increase.With(30))
store.Dispatch(c.Increase.With(20))
store.StateOf(c) // expect: 50
```
