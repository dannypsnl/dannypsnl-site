---
iscard: "true"
tags:
  - working
  - racket
  - data
title: "data type for racket"
link: "https://github.com/dannypsnl/data-type"
---

A brief example, you can find more in the repository and the [document](https://docs.racket-lang.org/data-type/).

```racket
(data E
  Number
  String
  (Add E E))
```
