---
iscard: "true"
tags:
  - racket
  - plt
  - macro
title: "controllable-refinement"
link: "https://github.com/dannypsnl/controllable-refinement"
---

A new language framework for substructural type system. The following shows how it forms a `sort` produces **sorted list**, `insert` might destroy the refinement, and `binary-search` only accepts a **sorted list**!

```racket
(: sort (-> (list {?+sorted} a) void))
(: insert (-> (list {?-sorted} a) a void))
(: binary-search (-> (list {sorted} a) a))
```
