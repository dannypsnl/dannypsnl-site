---
iscard: "true"
tags:
  - racket
  - dev tool
title: "re-algo"
link: "https://github.com/dannypsnl/re-algo"
---

A simple algorithm developing tool by showing internal state of program in time.
