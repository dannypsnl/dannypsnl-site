---
iscard: "true"
tags:
  - working
  - racket
  - ide
title: "sauron"
link: "https://github.com/racket-tw/sauron"
---

A DrRacket plugin to make it experience like an IDE

1. Refactoring
2. File explorer
3. Auto formatting
4. Jump to definition
