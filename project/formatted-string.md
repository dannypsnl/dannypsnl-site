---
iscard: "true"
tags:
  - racket
  - syntax extension
title: "formatted-string"
link: "https://github.com/dannypsnl/formatted-string"
---

Extends racket string to formatted string, example

```racket
(define-values (x y z) (values 1 2 3))
"x = $x, y = $y, z = $z, (+ x y z) = $(+ x y z)"
```
