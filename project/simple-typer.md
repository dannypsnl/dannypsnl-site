---
iscard: "true"
tags:
  - racket
  - nanopass
  - compiler
title: "simple-typer"
link: "https://github.com/racket-tw/simple-typer"
---

Demonstrate how to do type checking under nanopass framework.

Post link: https://racket-tw.github.io/post/2020-12-18-simple-type-check-with-nanopass.html
