# blog

- `posts/`: blog posts
- `css/`: style sheet
- `images/`: image
- `assets/`: other resource, e.g. pdf
- `org-template/`: org template file

```
make
make clean
```
