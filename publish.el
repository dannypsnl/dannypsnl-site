(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")
                         ("nongnu" . "http://elpa.nongnu.org/nongnu/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'htmlize)
(package-install 'org-contrib)
(with-temp-buffer
  (url-insert-file-contents
   "https://raw.githubusercontent.com/emacsmirror/ox-rss/80b5001c3d1c10b578d799bd8ff49e9267a2d5ff/ox-rss.el")
  (eval-buffer))

(require 'ox-publish)
(require 'ox-html)
(require 'ox-rss)

(setq org-fold-core-style 'overlay)

(setq org-src-fontify-natively t
      org-html-validation-link nil
      org-html-htmlize-output-type 'css
      org-html-doctype "html5"
      org-html-html5-fancy t)

(defun file-contents (filename)
  "Return the contents of FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(defun replace/org-publish-sitemap (title list)
  "Customized site map, as a string.
TITLE is the title of the site map.  LIST is an internal
representation for the files to include, as returned by
`org-list-to-lisp'.  PROJECT is the current project."
  (concat "#+title: " title "\n" (file-contents "./index.org") "\n\n"
          (org-list-to-org list)))

(setq org-export-global-macros
      '(("timestamp" . "@@html:<span class=\"timestamp\">[$1]</span>@@")))
(defun replace/org-sitemap-date-entry-format (entry style project)
  "Format ENTRY in org-publish PROJECT Sitemap format ENTRY ENTRY STYLE format that includes date."
  (let ((filename (org-publish-find-title entry project)))
    (if (= (length filename) 0)
        (format "*%s*" entry)
      (format "{{{timestamp(%s)}}} [[file:%s][%s]]"
              (substring entry 0 10)
              entry
              filename))))

; rss start
(defun rw/org-rss-publish-to-rss (plist filename pub-dir)
  "Publish RSS with PLIST, only when FILENAME is 'rss.org'. PUB-DIR is when the output will be placed."
  (if (equal "rss.org" (file-name-nondirectory filename))
      (org-rss-publish-to-rss plist filename pub-dir)))

(defun rw/format-rss-feed (title list)
  "Generate RSS feed, as a string.
TITLE is the title of the RSS feed.  LIST is an internal
representation for the files to include, as returned by
`org-list-to-lisp'.  PROJECT is the current project."
  (concat "#+title: " title "\n\n"
          (org-list-to-subtree list)))

(defun rw/format-rss-feed-entry (entry style project)
  "Format ENTRY for the RSS feed.
ENTRY is a file name.  STYLE is either 'list' or 'tree'.
PROJECT is the current project."
  (cond ((not (directory-name-p entry))
         (let* ((file (org-publish--expand-file-name entry project))
                (title (org-publish-find-title entry project))
                (date (format-time-string "%Y-%m-%d" (org-publish-find-date entry project)))
                (link (concat (file-name-sans-extension entry) ".html")))
           (with-temp-buffer
             (insert (format "* [[file:%s][%s]]\n" file title))
             (org-set-property "RSS_PERMALINK" link)
             (org-set-property "PUBDATE" date)
             ;(insert-file-contents file)
             (buffer-string))))
        ((eq style 'tree)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))
; rss end

(setq org-publish-project-alist
      '(("posts"
         :base-directory "posts/"
         :base-extension "org"
         :publishing-directory "dist/"
         :exclude "rss.org"
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title "Dan's Blog"
         :sitemap-filename "index.org"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :sitemap-format-entry replace/org-sitemap-date-entry-format
         :sitemap-function replace/org-publish-sitemap
         :author "Lîm Tsú-thuàn"
         :email "dannypsnl@gmail.com"
         :with-creator t)
        ("rss"
         :base-directory "posts/"
         :base-extension "org"
         :publishing-directory "dist/"
         :recursive nil
         :exclude "(rss|index)\.org"
         :publishing-function rw/org-rss-publish-to-rss
         :rss-extension "xml"
         :html-link-home "https://dannypsnl.me"
         :html-link-use-abs-url t
         :html-link-org-files-as-html t
         :auto-sitemap t
         :sitemap-filename "rss.org"
         :sitemap-title "Dan's Blog"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :sitemap-function rw/format-rss-feed
         :sitemap-format-entry rw/format-rss-feed-entry)
        ("css"
         :base-directory "css/"
         :base-extension "css"
         :publishing-directory "dist/css"
         :publishing-function org-publish-attachment
         :recursive t)
        ("images"
         :base-directory "images/"
         :base-extension "webp\\|svg\\|png\\|jpeg\\|gif"
         :publishing-directory "dist/images"
         :publishing-function org-publish-attachment
         :recursive t)
        ("assets"
         :base-directory "assets/"
         :base-extension "pdf"
         :publishing-directory "dist/assets"
         :publishing-function org-publish-attachment
         :recursive t)
        ("all" :components ("posts" "rss" "css" "images" "assets"))))
